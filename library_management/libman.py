'''
Created on Feb 16, 2016

@author: gin2

'''
from openerp import models, fields, api, _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as DT, DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.exceptions import except_orm, Warning, RedirectWarning
import time
from bson.json_util import default

Model = models.Model
Transient = models.TransientModel

'''
@Book: Inherited from model product.template with adding necessary fields. The goal is we will able to determine which one is a book.
'''

class Book(Model):
    _inherit = 'product.template'
    
    @api.onchange('is_book')
    def onchange_is_book(self):
        if self.is_book and self.is_book is True:
            self.type = 'product'
            self.rental = True
            ref = self.env['ir.model.data']
            
            dummy, uom_id = ref.get_object_reference('product', 'product_uom_unit')
            dummy, uos_id = ref.get_object_reference('product', 'product_uom_day')
            
            self.uom_id = uom_id
            self.uos_id = uos_id
            
        if self.is_book and self.is_book is False:
            self.rental = False
    
    is_book = fields.Boolean(string='is a Book')
    author_ids = fields.Many2many('res.partner', 'library_book_author_rel', 'book_id', 'partner_id', string='Authors')
    max_day = fields.Integer(string='Max. Order Days', default=1)
    max_qty = fields.Integer(string='Max. Order Qty', default=1)
    delay_charge = fields.Float(string='Delay Returning Charge', default=0.0)
    publisher_id = fields.Many2one('res.partner', string='Publisher')

class Attribute(Model):
    _inherit = 'product.attribute'
    
    book = fields.Boolean()

'''
@BookCart: to hold book selection data that will throw to Book Order. 
Similiar with Shopping Cart in online shopping
'''    
class BookCart(Model):
    _name = 'book.cart'
    _description = 'Book Cart'
    
    user_id = fields.Many2one('res.users', string='Borrower', required=True)
    book_id = fields.Many2one('product.product', string='Book', required=True, domain=[('is_book', '=', True)])
    qty = fields.Integer(string='Qty', default=1)
    days = fields.Integer(string='Days', default=1)
    

'''
@BookOrder: Document that states all the books that borrowed and contains related information
'''

class BookOrder(Model):
    _name = 'book.order'
    _description = 'Book order'
    
    @api.multi
    def action_confirm(self):
        # 1. Check again the availibility & rules
        for line in self.line_ids:
            line._check_qty()
            line._check_max_qty()
            line._check_max_day()
        
        # 1. UPDATE name's value to sequence number
        seq = self.env['ir.sequence'].get('book.order')
        self.name = seq
        
        
        # 2. DELETE CART (as not longer required)
        
        # call book.cart object
        cart_obj = self.env['book.cart']
        
        # filter according the user (who order)
        cart = cart_obj.search([('user_id.id', '=', self._uid)])
        # delete cart data
        cart.unlink()
        
        # 3. CREATE STOCK PICKING & TRIGGER WAREHOUSE STOCK RESERVING WORKFLOW
        # call stock.picking object
        picking = self.env['stock.picking']
        
        
        # define values values for picking. Default value for picking type is 'customer'
        # get Picking Type
        dummy, picking_type_id = self.env['ir.model.data'].get_object_reference('stock', 'picking_type_out')
        picking_type = self.env['stock.picking.type'].browse(picking_type_id)
        
        move_lines = []
        for line in self.line_ids:
            move_lines.append(
                              (0, 0,
                               {'name':line.book_id.name,
                                'product_id':line.book_id.id,
                                'product_uom_qty':line.qty,
                                'product_uom':line.book_id.uom_id.id,
                                'location_id':picking_type.default_location_src_id.id,
                                'location_dest_id':picking_type.default_location_dest_id.id,
                                }
                               )
                              )
        
        picking_vals = {
                        'partner_id':self.user_id.partner_id.id,
                        'picking_type_id':picking_type_id,
                        'origin':seq,
                        'move_lines':move_lines,
                        }
        
        pick_out = picking.create(picking_vals)
        pick_out.action_confirm()
        pick_out.action_assign()
        print "Picking ID %s" % (pick_out.id)
        self.picking_out_id = pick_out.id
        
        self.state = 'confirmed'
        
        
        
    @api.multi
    def action_taken(self):
        self.state = 'taken'
        # Change Book Order's state to 'taken'
        # TRIGGER WAREHOUSE DELIVERY WORKFLOW
        picking_id = self.picking_out_id.id
        return self.picking_out_id.do_enter_transfer_details_book()
    
    @api.multi
    def action_return(self):
        self.state = 'returned'
        context = {'active_id':self.picking_out_id.id}
        return self.pool['stock.return.picking'].create_returns(self._cr, self._uid, self.picking_out_id.ids, context=context)
        
    
    
    STATE = [('draft', 'Draft'), ('confirmed', 'Confirm'), ('taken', 'Taken'), ('returned', 'Returned')]
    
    name = fields.Char(string='Order Code', required=True, size=20, readonly=True, default='/')
    user_id = fields.Many2one('res.users', string='Borrower', required=True)
    line_ids = fields.One2many('book.order.line', 'order_id', string='Books')
    date = fields.Datetime()
    invoice_id = fields.Many2one('account.invoice', string='Invoice')
    picking_out_id = fields.Many2one('stock.picking', string='OUT')
    picking_in_id = fields.Many2one('stock.picking', string='IN')
    state = fields.Selection(STATE, string='State', required=True, readonly=True, default='draft')
    
class BookOrderLine(Model):
    _name = 'book.order.line'
    _description = 'Books that ordered'
    
    @api.model
    def _check_qty(self):
        if self.book_id.qty_available < self.qty:
            raise except_orm(_('Warning!'), _('Book: %s is not available this time or less than ordered qty!' % (self.book_id.name)))
    
    @api.model
    def _check_max_day(self):
        if self.book_id.max_day > self.days:
            raise except_orm(_('Warning!'), _('Book: %s could not borrowed more than %s days!' % (self.book_id.name, self.days)))
    
    @api.model
    def _check_max_qty(self):
        if self.book_id.max_qty > self.qty:
            raise except_orm(_('Warning!'), _('Book: %s could not borrowed more than %s item(s)!' % (self.book_id.name, self.qty)))
    
    @api.model
    def _get_price(self, book_id, user_res):
        pricelist_id = user_res.partner_id.property_product_pricelist.id
        book = self.env['product.product'].with_context({'pricelist':pricelist_id}).browse(book_id)
        return book.price
    
    
    @api.onchange('book_id', 'qty', 'days')
    def onchange_data(self):
        if self.book_id:
            self._check_qty()
            self.price = self._get_price(self.book_id.id, self.order_id.user_id)
        if self.qty:
            self._check_max_qty()
        if self.days:
            self._check_max_day()    
            
    
    @api.multi
    def _compute_all(self):
        for s in self:
            s.subtotal = s.qty * s.days * s.price
    
    STATE = [('draft', 'Draft'), ('taken', 'Taken'), ('returned', 'Returned')]
    CONDITION = [('good', 'Good'), ('damaged', 'Damaged')]
    
    order_id = fields.Many2one('book.order', string='Order', required=True, ondelete='cascade')
    book_id = fields.Many2one('product.product', string='Book', required=True, domain=[('is_book', '=', True)])
    days = fields.Integer(string='Days', default=1)
    qty = fields.Integer(default=1)
    price = fields.Float()
    subtotal = fields.Float(compute='_compute_all', string='Sub Total')
    returned_condition = fields.Selection(CONDITION, string='Returning Condition', default='good')
    return_condition_note = fields.Char(string='Returning Condition Note', size=255)
    state = fields.Selection(STATE, string='State', required=True, readonly=True, default='draft')
