
{
    'name' : 'Library Management',
    'version' : '1.1',
    'author' : 'Gin2',
    'category' : 'Custom',
    'description' : """""",
    'website': 'http://www.erpcentral.biz',
    'images' : [],
    'depends' : ['account', 'stock', 'product'],
    'data': [
             # INITIAL DATA
             'data/book_categ.xml',
             'data/seq.xml',
             'data/attribute.xml',
             
             # VIEWS
             'view/menu.xml',
             'view/book_order_view.xml',
             'view/book_view.xml',
             
             ],
    'qweb' : [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application':True
}
