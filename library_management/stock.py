
from openerp import models, fields, api, _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as DT, DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.exceptions import except_orm, Warning, RedirectWarning
import time

Model = models.Model
Transient = models.TransientModel

class Picking(Model):
    _inherit = 'stock.picking' 
    
    @api.model
    def do_enter_transfer_details_book(self):
        context = {
            'active_model': self._name,
            'active_ids': [self.id],
            'active_id': self.id
        }

        created = self.env['stock.transfer_details'].with_context(context).create({'picking_id': self.id})
        
        return created.with_context(context).wizard_view()