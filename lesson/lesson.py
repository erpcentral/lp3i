from openerp import models, fields, api, _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as DT, DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.exceptions import except_orm, Warning, RedirectWarning, ValidationError
import time

Model = models.Model
Transient = models.TransientModel

class Lesson(Model):
    _name = 'lesson.lesson'
    _description = 'Lesson'
    
    _INTERVAL = [('daily', 'Daily'), ('monthly', 'Monthly'), ('annually', 'Annually')]
    _STATE = [('draft', 'Draft'), ('cancel', 'Cancel'), ('done', 'Done')]
    
    @api.multi
    def action_draft(self):
        self.state = 'draft'
        
    @api.multi
    def action_cancel(self):
        self.state = 'cancel'
        
    @api.multi
    def action_done(self):
        self.state = 'done'
    
    @api.one
    def _get_yes(self):
        for s in self:
            s.yes = True
    
    name = fields.Char(string="Subject", size=20, required=True)
    lecturer_id = fields.Many2one('res.partner', string='Lecturer', required=True)
    book_ids = fields.Many2many('lesson.book', 'lesson_book_rel', 'lesson_id', 'book_id', string='Ref. Books')
    description = fields.Text(string='Description')
    time_start = fields.Datetime(string='Time Start', required=True)
    time_end = fields.Datetime(string='Time End', required=True)
    room_id = fields.Many2one('lesson.room', string='Room', required=True)
    interval = fields.Selection(_INTERVAL, string='Interval', required=True, default='monthly')
    state = fields.Selection(_STATE, string="State", readonly=True, default='draft')
    lecturer_email = fields.Char(string='Lecure Email', related='lecturer_id.email', readonly=1)
    yes = fields.Boolean(compute='_get_yes', string='Yes?') 
    
    @api.constrains('time_start', 'time_end')
    def _date_error(self):
        if self.time_end < self.time_start:
            raise ValidationError('Time End must be bigger than Time Start!!')

class Books(Model):
    _name = 'lesson.book'
    _description = 'Book for lesson'
    
    name = fields.Char(string="Title", size=20, required=True)

class Room(Model):
    _name = 'lesson.room'
    _description = 'Room'    
    
    name = fields.Char(string="Room", size=20, required=True)
    
    # ADD FIELDS : book references, room, duration (in hours)
    # WEEK 4
    """
    Adding more fields:
    1. Many2one -> res.partner (base module)
    2. Many2many -> lesson.book (new model in this module)
    3. Text -> lesson description
    4. datetime -> time start
    5. datetime -> time end
    6. many2one -> Room
    7. Selection -> Interval (daily, weekly, monthly, annualy)
    """
