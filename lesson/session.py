"""
a model to capture the attendance of students
in lecturing session/class
"""
from openerp import models, fields, api, _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT as DT, DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.exceptions import except_orm, Warning, RedirectWarning
import time

Model = models.Model
Transient = models.TransientModel

class Session(Model):
    _name = 'lesson.session'
    _description = 'Session'
    
    @api.multi
    def start_session(self):
        self.start_time = time.strftime(DT)
        self.state = 'started'
        
    @api.multi
    def end_session(self):
        self.end_time = time.strftime(DT)
        self.state = 'ended'
    
    STATE = [('draft', 'Draft'), ('started', 'Started'), ('ended', 'Ended')]
    
    lesson_id = fields.Many2one('lesson.lesson', string='Lesson', required=True)
    student_ids = fields.One2many('lesson.session.student', 'session_id', string='Student')
    start_time = fields.Datetime(string='Start', readonly=1)
    end_time = fields.Datetime(string='End', readonly=1)
    state = fields.Selection(STATE, string='State', readonly=1, default='draft')
    
class SessionStudent(Model):
    _name = 'lesson.session.student'
    
    @api.multi
    def sign_in(self):
        sgi = time.strftime(DT)
        self.sign_in_time = sgi
    
    session_id = fields.Many2one('lesson.session', string='Session', required=1)
    student_id = fields.Many2one('res.users', string='Student', required=1)
    sign_in_time = fields.Datetime(string='Time In')
    
