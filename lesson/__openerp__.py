{
    'name' : 'Lesson',
    'version' : '1.1',
    'author' : 'Gin2',
    'category' : 'Custom',
    'description' : """""",
    'website': 'http://www.erpcentral.biz',
    'images' : [],
    'depends' : [],
    'data': [
             'view/menu_view.xml',
             'view/lesson_view.xml',
             'view/book_view.xml',
             'view/room_view.xml',
             'view/session_view.xml',
             ],
    'qweb' : [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application':True
}